#!/bin/bash

# set -e
# (
# if lsof -Pi :27017 -sTCP:LISTEN -t >/dev/null ; then
#     echo "Please terminate the local mongod on 27017"
#     exit 1
# fi
# )

function clean_up {
    echo -e "\n\nSHUTTING DOWN\n\n"
    curl --output /dev/null -X DELETE http://localhost:8083/connectors/datagen-pageviews || true
    curl --output /dev/null -X DELETE http://localhost:8083/connectors/mongo-sink || true
    curl --output /dev/null -X DELETE http://localhost:8083/connectors/mongo-source || true
    docker-compose exec mongo1 /usr/bin/mongo --quiet --eval 'db.getMongo().getDBNames().forEach(function(i){db.getSiblingDB(i).dropDatabase()})'
    docker-compose down
    if [ -z "$1" ]
    then
      echo -e "Bye!\n"
    else
      echo -e $1
    fi
}

echo "Building dto(s) ..."
cd ../../common-model
mvn clean install
cd ../common/backend-lib
cd ../../event-driven-ms/product
mvn clean install -pl dto
cd ../../event-driven-ms/payment
mvn clean install -pl dto
cd ../../event-driven-ms/order
mvn clean install -pl dto
cd ../../event-driven-ms/user
mvn clean install -pl dto
cd ../../event-driven-ms/message
mvn clean install -pl dto

echo -ne "\n\nBuilding jar(s) ..."
cd ../../event-driven-ms/product
mvn clean install
cd ../../event-driven-ms/user
mvn clean install
cd ../../event-driven-ms/message
mvn clean install
cd ../../event-driven-ms/payment
mvn clean install
cd ../../event-driven-ms/order
mvn clean install

cd ../../common/backend-lib

echo "Starting docker."
docker-compose build
docker-compose up -d

sleep 10
echo -ne "\n\nWaiting for the systems to be ready.."
function test_systems_available {
  COUNTER=0
  until $(curl --output /dev/null --silent --head --fail http://localhost:$1); do
      printf '.'
      sleep 2
      let COUNTER+=1
      if [[ $COUNTER -gt 300 ]]; then
        MSG="\nWARNING: Could not reach configured kafka system on http://localhost:$1 \nNote: This script requires curl.\n"

          if [[ "$OSTYPE" == "darwin"* ]]; then
            MSG+="\nIf using OSX please try reconfiguring Docker and increasing RAM and CPU. Then restart and try again.\n\n"
          fi

        echo -e $MSG
        clean_up "$MSG"
        exit 1
      fi
  done
}

test_systems_available 8082
test_systems_available 8083

trap clean_up EXIT

# Mongo replica Set
echo -e "\nConfiguring the MongoDB ReplicaSet.\n"
docker-compose exec mongo1 /usr/bin/mongo --eval '''if (rs.status()["ok"] == 0) {
    rsconf = {
      _id : "rs0",
      members: [
        { _id : 0, host : "mongo1:27017", priority: 1.0 },
        { _id : 1, host : "mongo2:27017", priority: 0.5 },
        { _id : 2, host : "mongo3:27017", priority: 0.5 }
      ]
    };
    rs.initiate(rsconf);
}

rs.conf();'''

# Restoring mongoDB data
echo -e "\nRestoring MongoDB pre-defined data.\n"
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db product /data/product
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db user /data/user
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db order /data/order
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db message /data/message
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db payment /data/payment

# Source Connectors
echo -e "\nConfiguring kafka connect source connectors.\n"
curl -X POST -H "Content-Type: application/json" --data '
{
  "name": "mongo-order-source",
  "config": {
    "tasks.max": "1000",
    "connector.class": "com.mongodb.kafka.connect.MongoSourceConnector",
    "connection.uri": "mongodb://mongo1:27017,mongo2:27017,mongo3:27017",
    "topic.prefix": "com.ehy",
    "database": "order",
    "collection": "order",
    "pipeline": "[{\"$match\": {\"operationType\": {$in: [\"insert\", \"replace\"]}, \"fullDocument.status\": {$in: [\"ACTIVE\", \"CANCELLED\", \"EXPIRED\", \"FAILED\"]}}}, {\"$addFields\": {\"fullDocument.id\": {\"$toString\": \"$fullDocument._id\"}}}, {\"$project\": {\"fullDocument._id\": 0, \"fullDocument.createdAt\": 0, \"fullDocument.updatedAt\": 0}}]"
  }
}' http://localhost:8083/connectors -w "\n"

curl -X POST -H "Content-Type: application/json" --data '
{
  "name": "mongo-product-source",
  "config": {
    "tasks.max": "1000",
    "connector.class": "com.mongodb.kafka.connect.MongoSourceConnector",
    "connection.uri": "mongodb://mongo1:27017,mongo2:27017,mongo3:27017",
    "topic.prefix": "com.ehy",
    "database": "product",
    "collection": "inventory",
    "pipeline": "[{\"$match\": {\"operationType\": {$in: [\"insert\", \"replace\"]}}}, {\"$addFields\": {\"fullDocument.id\": {\"$toString\": \"$fullDocument._id\"}, \"fullDocument.date\": {\"$dateToString\": { \"format\": \"%Y-%m-%d\", \"date\": \"$fullDocument.date\" }}}}, {\"$project\": {\"documentKey\": 0, \"clusterTime\": 0, \"fullDocument._id\": 0, \"fullDocument.createdAt\": 0, \"fullDocument.updatedAt\": 0}}]"
  }
}' http://localhost:8083/connectors -w "\n"

curl -X POST -H "Content-Type: application/json" --data '
{
  "name": "mongo-payment-transaction-source",
  "config": {
    "tasks.max": "1000",
    "connector.class": "com.mongodb.kafka.connect.MongoSourceConnector",
    "connection.uri": "mongodb://mongo1:27017,mongo2:27017,mongo3:27017",
    "topic.prefix": "com.ehy",
    "database": "payment",
    "collection": "transaction",
    "pipeline": "[{\"$match\": {\"operationType\": {$in: [\"insert\", \"replace\"]}, \"fullDocument.status\": {$in: [\"CONFIRMED\", \"DENIED\", \"REFUNDED\"]}}}, {\"$addFields\": {\"fullDocument.id\": {\"$toString\": \"$fullDocument._id\"}}}, {\"$project\": {\"fullDocument.orderId\": 1, \"fullDocument.status\": 1, \"fullDocument.updatedBy\": 1, \"ns\": 1, \"operationType\": 1, \"_id\": 1}}]"
  }
}' http://localhost:8083/connectors -w "\n"

echo -e '''

==============================================================================================================
Examine the topics in the Kafka UI: http://localhost:9021 or http://localhost:8000/
Kafka Connect in http://localhost:8003/
Grafana in http://localhost:3000/
==============================================================================================================

Use <ctrl>-c to quit'''

read -r -d '' _ </dev/tty

#https://grafana.com/grafana/dashboards/893
#https://grafana.com/grafana/dashboards/8321

# HOW TO RESUME MONGODB KAFKA STREAM

#docker exec -it mongo1 bash
#var cursor = db.order.aggregate([{$changeStream: {}}])
#db.order.insert({})
#var resumeToken = cursor.next()['_id']
#resumeToken
#db.order.aggregate([{$changeStream: {resumeAfter: resumeToken}}])
