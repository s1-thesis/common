#!/bin/bash

# set -e
# (
# if lsof -Pi :27017 -sTCP:LISTEN -t >/dev/null ; then
#     echo "Please terminate the local mongod on 27017"
#     exit 1
# fi
# )

function clean_up {
    echo -e "\n\nSHUTTING DOWN\n\n"
    docker-compose exec mongo1 /usr/bin/mongo --quiet --eval 'db.getMongo().getDBNames().forEach(function(i){db.getSiblingDB(i).dropDatabase()})'
    docker-compose -f docker-compose-rest.yml down
    if [ -z "$1" ]
    then
      echo -e "Bye!\n"
    else
      echo -e $1
    fi
}

echo "Building dto(s) ..."
cd ../../common-model
mvn clean install
cd ../common/backend-lib
cd ../../rest/product
mvn clean install -pl dto
cd ../../rest/payment
mvn clean install -pl dto
cd ../../rest/order
mvn clean install -pl dto
cd ../../rest/user
mvn clean install -pl dto
cd ../../rest/message
mvn clean install -pl dto

echo -ne "\n\nBuilding jar(s) ..."
cd ../../rest/product
mvn clean install
cd ../../rest/user
mvn clean install
cd ../../rest/message
mvn clean install
cd ../../rest/payment
mvn clean install
cd ../../rest/order
mvn clean install

cd ../../common/backend-lib

echo "Starting docker."
docker-compose build
docker-compose -f docker-compose-rest.yml up -d

sleep 10

trap clean_up EXIT

# Mongo replica Set
# echo -e "\nConfiguring the MongoDB ReplicaSet.\n"
# docker-compose exec mongo1 /usr/bin/mongo --eval '''if (rs.status()["ok"] == 0) {
#     rsconf = {
#       _id : "rs0",
#       members: [
#         { _id : 0, host : "mongo1:27017", priority: 1.0 },
#         { _id : 1, host : "mongo2:27017", priority: 0.5 },
#         { _id : 2, host : "mongo3:27017", priority: 0.5 }
#       ]
#     };
#     rs.initiate(rsconf);
# }
#
# rs.conf();'''

sleep 15
# Restoring mongoDB data
echo -e "\nRestoring MongoDB pre-defined data.\n"
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db product /data/product
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db user /data/user
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db order /data/order
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db message /data/message
docker exec -it mongo1 mongorestore --gzip --host localhost --port 27017 --db payment /data/payment

echo -e '''

==============================================================================================================
Open up grafana in http://localhost:3000/
==============================================================================================================

Use <ctrl>-c to quit'''

read -r -d '' _ </dev/tty

#https://grafana.com/grafana/dashboards/893
#https://grafana.com/grafana/dashboards/8321
